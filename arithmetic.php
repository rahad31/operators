<h2>Arithmetic Operator</h2>
<?php

echo (5 % 3)."<br>";           // prints 2
echo (5 % -3)."<br>";          // prints 2
echo (-5 % 3)."<br>";          // prints -2
echo (-5 % -3);         // prints -2

?>
