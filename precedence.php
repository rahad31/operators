<h2>Operator Precedence</h2>

<?php
echo $a = 3 * 3 % 5; // (3 * 3) % 5 = 4
// ternary operator associativity differs from C/C++
echo '<br>';
echo $a = true ? 0 : true ? 1 : 2; // (true ? 0 : true) ? 1 : 2 = 2
echo '<br>';
echo $a = 1;
echo '<br>';
echo $b = 2;
echo '<br>';
echo $a = $b += 3; // $a = ($b += 3) -> $a = 5, $b = 5
?>

