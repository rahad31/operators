<h2>Array Operator</h2>
<?php
$a = array("a" => "apple", "b" => "banana");
$b = array("a" => "pear", "b" => "strawberry", "c" => "cherry");

$c = $a + $b; // Union of $a and $b
echo "Union of \$a and \$b: <br>";
var_dump($c);

$c = $b + $a; // Union of $b and $a
echo "Union of \$b and \$a: <br>";
var_dump($c);

$a += $b; // Union of $a += $b is $a and $b
echo "Union of \$a += \$b: <br>";
var_dump($a);
?>
